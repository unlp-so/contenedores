SLIDE_FMT: main

# Fundamentos
## de contenedores
----
## Objetivo

Este curso introduce los conceptos fundamentales detrás de los contenedores,
abordándolos desde su estructura e identificando sus características principales
en términos de la inmutabilidad. En este sentido, haremos principal hincapié en
su uso de forma correcta a través de recursos que se proveen en torno a ellos.

Al finalizar, se espera el alumno se familiarice con las tecnologías de
contenedores contractuales, su uso, creación y aplicación de buenas prácticas.

----

## Agenda

* ¿Qué son los Contenedores?
* Precedentes
* docker cli
* Construcción de imágenes
* Registry de imágenes
* Persistencia de datos
* Redes
* Gestión de recursos
* Docker daemon
* Compose
* Contenedores sin docker
* Orquestación de contenedores

---

FILE: sections/contenedores.md

---
FILE: sections/historia-contenedores.md

---

FILE: sections/docker-cli.md

---

FILE: sections/construyendo-imagenes.md

---

FILE: sections/registries.md

---

FILE: sections/persistencia.md

---

FILE: sections/redes.md

---

FILE: sections/limites.md

---

FILE: sections/servicio-docker.md

---

FILE: sections/compose.md

---

FILE: sections/docker-alternativas.md

---

FILE: sections/clusters.md
