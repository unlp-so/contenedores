SLIDE_FMT: new-topic

# Construyendo imágenes

----
<!-- .slide: data-auto-animate -->

## Dockerfile

* Construcción de imágenes de forma declarativa.
* Utiliza un archivo de texto plano para definir el contenido de una imagen.
  * Permite escribir instrucciones a ejecutar.
* Automatiza el proceso de la creación de imágenes.
* Permite repetir y modificar fácilmente una imagen.
* Generar de forma simple imágenes derivadas.

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile

En una carpeta creamos un archivo **`Dockerfile.hello`** con el siguiente contenido:

CODE: dockerfile/hello/Dockerfile.hello bash 1-10

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile


CODE: dockerfile/hello/Dockerfile.hello bash 1-1

**`FROM`** nos permite heredar de una imagen.
----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile


CODE: dockerfile/hello/Dockerfile.hello bash 4-6

**`RUN`** ejecuta comandos en la imagen usando contenedores para ir generando
capas.

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile


CODE: dockerfile/hello/Dockerfile.hello bash 8-8

**`EXPOSE`** indica qué puerto este contenedor expone. Utilizado por servicios
de autodiscovery.

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile

CODE: dockerfile/hello/Dockerfile.hello bash 9-9

**`CMD`** comando que se ejecuta cuando el contenedor se inicia sin argumentos.

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Dockerfile

Generamos la imagen con **`build`**

```bash
docker build -t unlp-so/helloworld:1.0.0 \
  -f Dockerfile.hello .
```

<div class="small fragment">

Usamos la opción **`-f`** porque el nombre no sigue la convención: en vez de
llamarse `Dockerfile` se llama `Dockerfile.hello`. 

El punto al final indica el contexto utilizado para generar la imagen.

</div>
<div class="fragment">

La nueva imagen quedará _en nuestro sistema_ **lista para
lanzar contenedores**.

```bash
docker image ls
```
</div>

----
## Probamos la nueva imagen

```bash
docker run --rm -d -p 80:80 unlp-so/helloworld:1.0.0
```

<div class="fragment">

Verificamos:

```bash
curl localhost
docker ps -lq | xargs docker rm -f
```

----
## Reuso de capas

Generamos nuevamente la imagen a partir del mismo `Dockerfile` para verificar si
vuelve a hacer lo mismo o reusa lo hecho:

```bash
docker build -t unlp-so/helloworld:1.1.0 \
  -f Dockerfile.hello .
docker image ls
```
<div class="small fragment">

**Observar el `IMAGE ID` de las imagenes con tag 1.0.0 y 1.1.0**.

Observar también que cada línea que se ejecuta del Dockerfile dice **`CACHED`**.
Puede ser necesario correr el comando `docker build --progress plain`
</div>

----
## history

```bash
docker image history unlp-so/helloworld:1.0.0
docker image history unlp-so/helloworld:1.1.0
```

<div class="small fragment">

Podemos observar que ambos comandos comparten IMAGE ID de cada capa de la
imagen.

</div>

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Nuestra imagen usaba nginx para servir un sitio estático. Vamos a utilizar,
la imagen oficial de nginx basada en [alpine linux](https://alpinelinux.org/).

Creamos entonces un nuevo **`Dockerfile`**:

CODE: dockerfile/hello.nginx/Dockerfile bash 1-4

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Nuestra imagen usaba nginx para servir un sitio estático. Vamos a utilizar,
la imagen oficial de nginx basada en [alpine linux](https://alpinelinux.org/).

Creamos entonces un nuevo **`Dockerfile`**:

CODE: dockerfile/hello.nginx/Dockerfile bash 1-1

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Verificamos y probamos la nueva imagen

```bash 
docker build -t unlp-so/helloworld:1.2.0 .
```
<div class="small">
Construimos la imagen
</div>

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Verificamos y probamos la nueva imagen

```bash 
docker image ls
```
<div class="small">
Verificamos la imagen
</div>

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Verificamos y probamos la nueva imagen

```bash 
docker run -d --rm -p 80:80 unlp-so/helloworld:1.2.0
```
<div class="small">
Corremos el contenedor
</div>

----
<!-- .slide: data-auto-animate -->

## Refactorizamos nuestra imagen

Verificamos y probamos la nueva imagen

```bash 
curl localhost
docker ps -lq | xargs docker rm -f
```
<div class="small">
Verificamos funciona y eliminamos el contenedor
</div>

----
<!-- .slide: data-auto-animate -->
## CMD y ENTRYPOINT

* Estas directivas permiten especificar qué comando se ejecutará de forma
  predeterminada al instanciar una imagen.
* Además, depende de cómo se empleen pueden tener diferente significado

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Vamos a explicarlo con ejemplos. Creamos una imagen sin CMD ni ENTRYPOINT:

CODE: dockerfile/no-cmd-nor-entrypoint/Dockerfile 1-10


----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Construimos y probamos:

```bash
docker build -t unlp-so/mute .
docker run --rm unlp-so/mute
```

<div class="fragment small">
Arroja el siguiente error:

```bash
docker: Error response from daemon: No command specified.
See 'docker run --help'.
```
</div>

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Si probamos ahora

```bash
docker run --rm unlp-so/mute hostname
docker run --rm unlp-so/mute echo 'Hola mundo'
docker run --rm unlp-so/mute ping localhost -c 3
```
----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Agregamos al dockerfile **`CMD`** y probamos nuevamente 

CODE: dockerfile/no-cmd-nor-entrypoint/Dockerfile.withcmd 1-10

<div class="fragment">

Y construimos la nueva imagen

```bash
docker build -t unlp-so/mute-with-cmd -f Dockerfile.withcmd .
docker run --rm unlp-so/mute-with-cmd
docker run --rm unlp-so/mute-with-cmd hostname
```
</div>
<div class="fragment small">

Al parecer CMD es el argumento que enviamos al contenedor cuando corremos
**`docker run IMAGE CMD`**.
</div>
----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Agregamos al dockerfile **`ENTRYPOINT`** y probamos

CODE: dockerfile/no-cmd-nor-entrypoint/Dockerfile.withentrypoint 1-10

<div class="fragment">

Y construimos la nueva imagen

```bash
docker build -t unlp-so/mute-with-entrypoint \
  -f Dockerfile.withentrypoint .
docker run --rm unlp-so/mute-with-entrypoint
```
</div>
----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Pero cuando probamos estos casos con **`ENTRYPOINT`**:

```bash
docker run --rm unlp-so/mute-with-entrypoint hostname
docker run --rm \
  --entrypoint hostname unlp-so/mute-with-entrypoint
```
<div class="fragment small">

Vemos que el ENTRYPOINT no se modifica con el comando que es argumento a
**`docker run`**.  ENTRYPOINT sólo se modifica con la opción
**`--entrypoint`**
</div>
----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Dado que es mucho más cómodo modificar CMD que ENTRYPOINT, se **recomienda**
utilizar CMD en el Dockerfile cuando se desea flexibilizar al usuario la
modificación del CMD que provee nuestra imagen.

En contraste, ENTRYPOINT se utiliza cuando los contenedores se deben comportar
de forma exclusiva. Esto es, que los usuarios de la imagen no modifiquen el
ejecutable definido.

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Es común utilizar como ENTRYPOINT comandos que se empaquetan como imágenes.
Luego, correr la imagen con **`docker run`** debería ofrecer las mismas
características que el script instalado localmente. 

_Podría haberse utilizado CMD, pero utilizar ENTRYPOINT envía un mensaje mas
fuerte_, indicando que el contenedor debe utilizarse sólo con ese script.

<div class="fragment small">

```bash
docker run --rm  bitnami/jsonnet:latest 
docker run --rm  bitnami/jsonnet:latest -e '{"hello": 1+2 }'
```
[Ver como es este Dockerfile](https://hub.docker.com/r/bitnami/jsonnet/)
</div>

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Las instrucciones CMD y ENTRYPOINT soportan dos formatos llamados:

**SHELL:** especificando los comandos como string

```bash
CMD executable param1 param2
```

**EXEC:** especificando los comandos como arreglos de strings

```bash
CMD ["executable","param1","param2"]
```
----

<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Analizamos el formato SHELL con la imagen **`unlp-so/mute-with-cmd`**

```bash
docker run --rm -d unlp-so/mute-with-cmd
docker ps -l --format '{{.Command}}'
```

<div class="fragment small">

Vemos que el comando corre en el contexto de **`sh -c CMD`**. No todas las
imágenes disponen de un shell.

Además, dependiendo del shell puede que el PID del proceso que corremos no
necesariamente sea el 1.
</div>

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

<div class="small container">

<div class="col">

CODE: dockerfile/shell-cmd/Dockerfile.dash 1-10

`Dockerfile.dash`


```bash
docker build \
  -t unlp-so/shell-dash \
  -f Dockerfile.dash .
docker run --rm --name dash \
  -d unlp-so/shell-dash
docker exec dash ps -ef
```

<div class="fragment">

_Vemos que el PID 1 es el del shell que lanza **`tail`**._
</div>
</div>
<div class="col">

CODE: dockerfile/shell-cmd/Dockerfile.bash 1-10


`Dockerfile.bash`

```bash
docker build \
  -t unlp-so/shell-bash \
  -f Dockerfile.bash .
docker run --rm --name pid1 \
  -d unlp-so/shell-bash
docker exec pid1 ps -ef
```
<div class="fragment">

_Vemos que el PID 1 es el del comando **`tail`**._
</div>
</div>
</div>

> [Dash](https://es.wikipedia.org/wiki/Debian_Almquist_Shell) es un shell liviano creado por debian.

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Analizamos el formato EXEC

```bash
FROM alpine
CMD ["/bin/ping","localhost"]
```


<div class="small">

Cuando se utiliza este formato el comando se ejecutará sin un shell.

**Sin importar si se utiliza como CMD o ENTRYPOINT, el formato recomendado
siempre es EXEC.**
</div>

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Las instrucciones ENTRYPOINT y CMD **pueden combinarse**:

CODE: dockerfile/entrypoint-and-cmd/Dockerfile 2-3

```bash
docker build -t unlp-so/ping .
docker run --rm unlp-so/ping
docker run --rm unlp-so/ping www.google.com.ar
```
<div class="small fragment">

Vemos que el **CMD** se agrega como argumento de **ENTRYPOINT**.
</div>

<div class="small fragment">

¿Es correcto el uso de la opción **`-c 10`** en el entrypoint?
</div>

----
<!-- .slide: data-auto-animate -->

## CMD y ENTRYPOINT

Veamos qué sucede con el mismo ejemplo usando el formato SHELL

CODE: dockerfile/entrypoint-and-cmd/Dockerfile.shell 2-3


```bash
docker build -t unlp-so/ping-shell -f Dockerfile.shell .
docker run unlp-so/ping-shell
```
<div class="fragment">

**No funciona porque:**

```bash
docker inspect $(docker ps -lq) --format \
  '{{ .Path }} {{ join .Args " "  }}'
```
</div>

----

## El PID 1

El PID 1 tiene un significado especial en Linux. Es el PID del proceso
_**init**_  y por ello tiene otras responsabilidades, como por ejemplo adoptar
procesos huérfanos y manejar zombies.

Típicamente un proceso que recibe la señal **SIGTERM** y no implementa un
manejador para esa señal tiene el comportamiento de _**terminar de forma inmediata**_.
Sin embargo, **_si el PID 1 recibe esta señal_** y no implementa un manejador, **no hará
nada**.

----

## Verficamos comportamiento PID1

```bash
docker exec -it pid1 bash
```

Analizamos en la consola:

```
ps -ef
kill -INT 1
tail -f /dev/null &
PID=$!
ps -ef
kill -INT $PID
ps -ef
```
<div class="fragment small">

El comando **`tail`** no implementa un manejador de señales. Es por ello que
**CTRL+C** enviado al contenedor iniciando `tail` como PID 1 no funciona, pero
sí cuando no es PID 1.
</div>
----
<!-- .slide: data-auto-animate -->

## Contexto de build

Cuando ejecutamos el comando **build** vimos que se especifica con **`-f`** el
path al **`Dockerfile`** y luego un path -_que generalmente es un punto,
significando el directorio actual_- que indica el **contexto**.

El contexto es utilizado diferente por docker build dependiendo de si se
habilita o no [buildkit](https://docs.docker.com/build/buildkit/).

----
<!-- .slide: data-auto-animate -->

## Contexto de build


* Sin Buildkit, el contexto siempre es enviado al servicio docker para
  construir la imagen.
* Con buildkit, el contexto se envía [sólo si en el Dockerfile se utiliza
  COPY](https://docs.docker.com/build/building/context/).

Cuando queremos evitar se envíe determinado archivo, podemos usar
**`.dockerignore`**.

----

## COPY o ADD

Para poder agregar archivos en las imágenes se utilizan las instrucciones **`
COPY`** o **`ADD`**. ¿Dos instrucciones para hacer lo mismo?

* `ADD` admite agregar URLs remotas o archivos en formato tar. Útil en casos
  como por ejemplo `ADD rootfs.tar.xz /`.
* `COPY` admite únicamente copiar archivos o carpetas del contexto.

<div class="fragment">

Es preferible utilizar **`COPY`** porque es más intuitivo que `ADD` en cuanto a lo que
hace la instrucción.

</div>

----
<!-- .slide: data-auto-animate -->

## Buenas prácticas con COPY y ADD

Cuando creamos una imagen que requiere incluir varios archivos del contexto,
conviene incluirlos por separado con algún criterio. Por ejemplo:

<pre><code class='bash hljs' data-trim data-line-numbers>
COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt
COPY . /tmp/
</code></pre>

----
<!-- .slide: data-auto-animate -->

## Buenas prácticas con COPY y ADD

Cuando creamos una imagen que requiere incluir varios archivos del contexto,
conviene incluirlos por separado con algún criterio. Por ejemplo:

<pre><code class='bash hljs' data-trim data-line-numbers="2">
COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt
COPY . /tmp/
</code></pre>

<div class="small fragment">

En este ejemplo, los requerimientos no suelen cambiar. Entonces la regeneración
de la imagen podrá utilizar la cache que ya tiene instaladas las dependencias
</div>

----
<!-- .slide: data-auto-animate -->

## Buenas prácticas con COPY y ADD

Utilizar `curl` o `wget` en favor de `ADD`. En vez de:

```bash
ADD https://example.com/big.tar.xz /usr/src/things/
RUN tar -xJf /usr/src/things/big.tar.xz -C /usr/src/things
RUN make -C /usr/src/things all
```

Se recomienda:

```bash
RUN mkdir -p /usr/src/things \
    && curl -SL https://example.com/big.tar.xz \
    | tar -xJC /usr/src/things \
    && make -C /usr/src/things all
```
----

## Minimizar el número de capas

Para no generar imágenes muy grandes se proveen las siguientes características:

* Sólo las instrucciones `RUN`, `COPY` y `ADD` crearán capas. _Otras
  instrucciones crearán capas intermedias que no incrementarán el tamaño del
  build_.
* Cuando sea posible, utilizar **multistage builds** para únicamente copiar los
  artefactos en la imagen final. _Esto permitirá instalar compiladores,
  herramientas de debug y demás extras que no serán necesarias en la imagen
  final_.
----
<!-- .slide: data-auto-animate -->

## Ejemplo

CODE: dockerfile/php/Dockerfile 1-10

Construimos la imagen:
```bash
docker build -t unlp-so/php-sample .
docker run --rm -d -p 9090:80 unlp-so/php-sample
curl localhost:9090
```

----
<!-- .slide: data-auto-animate -->

## Ejemplo


CODE: dockerfile/php/Dockerfile.enhanced 1-10

> Agregamos soporte para mysql, ldap y postgres. Incrementamos la memoria a 512MB.
<!-- .element: class="fragment" -->

----
<!-- .slide: data-auto-animate -->

## Ejemplo


```bash
docker build -t unlp-so/php-sample:enhanced \
  -f Dockerfile.enhanced .
docker run --rm -d -p 9091:80 \
  unlp-so/php-sample:enhanced
curl localhost:9091
```
----

## Analizamos las capas

```bash
docker history unlp-so/php-sample:enhanced
```

----
<!-- .slide: data-auto-animate -->
## Multistage builds


CODE: multistage/html2md/Dockerfile 1-10

<div class="small">
La idea es utilizar diferentes imágenes temporales para generando artefactos que
podrán utilizarse por otras imágenes.
</div>

```bash
docker build -t unlp-so/html2md .
docker run --rm -it unlp-so/html2md \
  -i https://unlp.edu.ar
```
