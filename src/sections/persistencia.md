SLIDE_FMT: new-topic

# Volúmenes

----
## ¿Cómo guardo la información?

* Los contenedores son volátiles e inmutables.
* Debemos preservar la información importante.
* ¿Dónde?

<div class="fragment">

**En volúmenes de datos.**
</div>

----
## Características de los volúmenes

* No utilizan un sistema de archivos de unión (UFS).
* Son más performantes que utilizar la capa RW del contenedor (UFS).
* Pueden compartirse y reusarse entre contenedores.
* Los cambios se hacen directamente en el volumen.
* La información del volumen **no se incluye** en la imagen.
* Persisten aún cuando se eliminen todos los contenedores que los usan.
  * Pueden quedar volúmenes sin referenciar.

----
<!-- .slide: data-auto-animate -->
## Tipos de volúmenes

* **[Bind mounts](https://docs.docker.com/storage/bind-mounts/):** son
  directorios del propio filesystem en el SO host.
* **Volúmenes manejados por docker:** que pueden ser anónimos o nombrados.
* Si se utiliza **Linux** también pueden utilizarse montajes de tipo **tmpfs**.

<img class="fragment" src="images/types-of-mounts-volume.png" height="200px" />

----
<!-- .slide: data-auto-animate -->
## Tipos de volúmenes

* Los **volúmenes** se almacenan en una parte del filesystem que _administra
  docker_ (`/var/lib/docker/volumes`). **Son la mejor forma de persistir datos
  en Docker**.
* Los **bind mounts** pueden utilizar cualquier directorio del sistema host.
  Incluso podrían importar directorios importantes como ser: `/etc`,
`/var/run/docker.sock`, etc.
* **`tmpfs`** se almacenan en memoria del sistema host y nunca escriben en archivos.
  * Usado internamente por los servicios swarm para montar **_secrets_**.
----
## ¿Volúmenes o bind mounts?

* Los volúmenes se manejan mediante docker cli.
* Los volúmenes funcionan en Linux y Windows.
* Los volúmenes pueden compartirse de forma más segura entre diferentes
  contenedores.
* Los drivers de volúmenes permiten almacenar los volúmenes en hosts remotos o
  servicios de la nube, encriptar su contenido, etc.
* Un nuevo volúmen puede inicializarse con datos por un contenedor.

<div class="small"> 

Si un contenedor utilizará información que no deba
persistirse se recomienda utilizar **tmpfs**. Los bind mounts pueden especificar **[bind propagation](https://docs.docker.com/storage/bind-mounts/#configure-bind-propagation)**.</div>

----
<!-- .slide: data-auto-animate -->
## ¿Volúmenes o bind mounts?

* Al montar un volumen la información que exista en el punto de montaje se
  copia al volumen.
* Con bind mounts, o cuando se monta el filesystem desde otro contenedor, se oculta la
  información que exista en el punto de montaje.
  * Correspondencia con el comando **`mount`**.

----
## Volúmenes

* Originalmente se utilizaba **`-v`** ó **`--volume`** para correr contenedores
  standalone, es decir, con **`docker run`**.
* En modo swarm, los servicios usaban **`--mount`**.

<div class="small fragment">

Pero a partir de docker 17.06 `--mount` puede usarse por contenedores
standalone:

* Son el mecanismo preferido y recomendado por su expresividad y simplicidad 
* `-v` combina todas las opciones en un campo mientras que `--mount` los separa.

</div>
----
<!-- .slide: data-auto-animate -->
## Volúmenes

* **Opciones cuando se utiliza `-v`:** mantiene tres campos separados por
  **`:`**. Los campos deben respetar el orden y su significado a priori no es
obvio:
  * El primer campo corresponde al nombre del volumen. Si se omite es un volumen
    anónimo.
  * El segundo campo es el path que el contenedor utilizará para montar el
    volúmen.
  * El tercer campo es opcional y corresponde a una lista de opciones separadas
    por coma como por ejemplo **`ro`**.

----
<!-- .slide: data-auto-animate -->
## Volúmenes

<div class="small">

* **Opciones cuando se utiliza `--mount`:** consiste de múltiples pares
  clave-valor separados por coma. La sintaxis es más expresiva que su
  contraparte y el orden de las opciones no importa. Los valores que pueden
  especificarse son:
  * **`type`:** puede ser `bind`, `volume` o `tmpfs`.
  * **`source`:** para volúmenes nombrados un nombre. En volúmenes anónimos se
    omite. Puede usarse **`src`**.
  * **`destination`:** punto de montaje en el contenedor. Puede usarse **`dst`**
    o **`target`**.
  * **`readonly`:** monta el volumen como solo lectura. No es necesario
    especificar un valor.
  * **`volume-opts`:** puede especificarse múltiples veces. Toma un par key-value
    con la opción y valor.
</div>
----
<!-- .slide: data-auto-animate -->
## Volúmenes

<div class="container">

<div class="col">

```bash

docker volume create \
  my-vol

```
**Crear**
</div>
<div class="col fragment">

```bash

docker volume ls


```
**Listar**
</div>
<div class="col fragment">

```bash

docker volume rm \
  my-vol

```
**Eliminar**
</div>
</div>

----
<!-- .slide: data-auto-animate -->
## Volúmenes

<div class="small"> 

```bash
docker inspect my-vol
```


```json
[
    {
        "CreatedAt": "2020-10-22T07:43:37-03:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/my-vol/_data",
        "Name": "my-vol",
        "Options": {},
        "Scope": "local"
    }
]
```
</div>
----
### Creamos un contenedor con un volumen

<div class="small">

Creamos un contenedor con un **volumen anónimo**. Ejemplificamos con ambas
sintaxis.
</div>
<div class="container">
<div class="col">

```bash

docker run -it -v /opt alpine

```
</div>
<div class="col">

```bash
docker run -it \
  --mount dst=/opt alpine
```
</div>
</div>

<div class="small fragment">

```bash
ls /opt/
echo "Prueba" > /opt/archivo
exit
```

Una vez con el contenedor finalizado lo inspeccionamos para determinar su volumen

<div class="container">
<div class="col">

```bash
docker inspect $(docker ps -alq) \
  --format '{{json .Mounts}}' | jq
```

</div>
<div class="col">

```bash
docker inspect $(docker ps -alq) \
  --format '{{range .Mounts}}{{ .Name }}{{end}}'
```
</div>
</div>
</div>
----
<!-- .slide: data-auto-animate -->
## Inspeccionamos el volumen

```bash
docker inspect $(docker ps -alq) \
  --format '{{range .Mounts}}{{ .Name }}{{end}}' | \
  xargs docker volume inspect
```
<div class="small fragment">

```json
[{
  "CreatedAt": "2020-10-22T07:58:53-03:00",
  "Driver": "local",
  "Labels": {
      "com.docker.volume.anonymous": ""
  },
  "Mountpoint": "/var/lib/docker/volumes/ee46d3461c87f63c4682c09b316489d6d5a337b48c55ea203c36c75282c90c66/_data",
  "Name": "ee46d3461c87f63c4682c09b316489d6d5a337b48c55ea203c36c75282c90c66",
  "Options": null,
  "Scope": "local"
}]
```
</div>
----
<!-- .slide: data-auto-animate -->
## Inspeccionamos el volumen

```bash
sudo ls $(docker inspect $(docker ps -alq) \
   --format '{{range .Mounts}}{{ .Source }}{{end}}')
```
----
<!-- .slide: data-auto-animate -->
## Inspeccionamos el volumen

```bash
sudo cat $(docker inspect $(docker ps -alq) \
   --format '{{range .Mounts}}{{ .Source }}{{end}}')/archivo
```

----
## Volúmenes anónimos

¿Qué sucede si ejecutamos?

```bash
docker run -it -v /opt alpine ls /opt
```

<div class="small fragment">

¿Esperábamos ver el archivo antes creado pero el resultado es vacío? **¿Por qué?**
</div>

----
<!-- .slide: data-auto-animate -->
## Volúmenes anónimos

Los volúmenes anónimos se eliminan cuando:

* Se corre **`docker run --rm`**. 
* Si el contenedor asociado al volumen usa **`docker rm -v`**.
* Explícitamente usando **`docker volume rm`**.

----
<!-- .slide: data-auto-animate -->
## Volúmenes nombrados

<div class="container">
<div class="col">

```bash
docker run -it --rm \
  -v test:/opt alpine
```
</div>
<div class="col">

```bash
docker run -it --rm \
  --mount src=test,dst=/opt alpine
```
</div>
</div>

<div class="small">

Observar el uso de **`--rm`**.
</div>

<div class="small fragment">

```bash
ls /opt/
echo "Prueba" > /opt/archivo
exit
```

Verificamos el contenido del volumen nombrado:

<div class="container">
<div class="col">

```bash

docker volume inspect test

```

</div>
<div class="col">

```bash
sudo ls $(docker volume inspect test \
  --format '{{ .Mountpoint }}')
```
</div>
</div>
</div>
----
<!-- .slide: data-auto-animate -->
## Volúmenes nombrados

Es posible reutilizarlos

```bash
docker run --rm -it -v test:/opt alpine ls /opt/
```

----
## Uso de bind mounts

Creamos un directorio en el sistema operativo host.

```bash
mkdir -p /tmp/data && ls /tmp/data
```

Ejecutamos el contenedor montando el directorio creado.

<div class="container">
<div class="col">

```bash
docker run --rm -it \
  -v /tmp/data:/opt alpine
```

</div>
<div class="col">

```bash
ls /opt/
```

<div class="fragment small">

Dentro del contenedor, vemos que nada existe en `/opt`.
</div>

</div>
</div>
----
<!-- .slide: data-auto-animate -->
## Uso de bind mounts


Desde el host, creamos un archivo con contenido.

```bash
echo "Prueba" > /tmp/data/archivo
```

<div class="fragment">

Verificamos `/opt` en el contenedor

```bash
ls /opt/
```

</div>
----
<!-- .slide: data-auto-animate -->
## Uso de bind mounts

Agregamos contenido desde el contenedor

```bash
echo "Otra prueba" >> /opt/archivo
```

Finalmente, en el host podemos verificar el archivo fue actualizado.


----
<!-- .slide: data-auto-animate -->
## Uso de bind mounts

* Cuando se usan volúmenes montados desde el host, no se crea ningún
  volumen de Docker.
  * Igual lógica que al montar un recurso en un equipo Linux.
* Se pisa el contenido

```bash
docker run --rm -v /tmp/data:/bin alpine
```
<div class="fragment"> 

**¿Qué pasaría?** 
</div>
<div class="fragment"> 

**_Probar con un volumen_**
</div>

----

## Volúmenes desde otro contenedor


```bash
docker run -v /opt --name alpine-test alpine \
  sh -c 'echo Hola > /opt/ejemplo'

docker run --rm  --volumes-from alpine-test alpine \
  cat /opt/ejemplo

docker volume ls && \
  docker rm -v alpine-test && \
  docker volume ls
```

----

## Prune

El siguiente comando eliminará aquellos volúmenes que no estén attachados a
ningún contenedor:

```bash
docker volume prune
```
