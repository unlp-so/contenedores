SLIDE_FMT: new-topic

# Orquestación de contenedores

----

## Introducción

La idea detrás de los clusters de contenedores es la de disponer de nodos
físicos con algún runtime que les permita correr contenedores.

----
<!-- .slide: data-auto-animate -->
## Características

* Diseño descentralizado
  * Tolerantes a fallas
  * Alta disponibilidad
  * Balanceo de carga
* Roles definidos
  * Master
  * Workers
* Servicios o pods
  * No simples contenedores.
  * ¿Contenedores?
  * Escalables

----
<!-- .slide: data-auto-animate -->
## Características

* Scheduling
  * Distribuir la ejecución de contenedores entre un pool de workers.
  * Recuperación ante fallas.
  * Rebalanceo de contenedores cuando fallan workers.
  * Escalamiento de contenedores.
* Health checks
  * Determinar el estado de servicios: usando comandos, HTTP, TCP. A veces para
    determinar si está sano un servicio, otras veces para conocer si puede
    recibir peticiones.
  * Autohealing.
----
<!-- .slide: data-auto-animate -->
## Características
* Service discovery
  * Crear nuevos servicios no tiene sentido si no es simple encontrarlos.
* Balanceo de requerimientos entre servicios internos o de ingreso al cluster.
* Attach de varios tipos de storage a contenedores dentro del cluster.

----
## Productos

Hoy día el cluster de contenedores más popular es
[Kubernetes](https://kubernetes.io/).

Docker por su parte mantiene [Swarm](https://docs.docker.com/engine/swarm/).
