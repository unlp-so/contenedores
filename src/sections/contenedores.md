SLIDE_FMT: new-topic

# ¿Qué son los contenedores?

----
<!-- .slide: data-auto-animate -->

## Analogía con el transporte

Antiguamente, el transporte de mercadería:


<div class="container" >

<div class="col" style="min-width: 65%">

* Diferentes tamaños, formas, resistencias, etc.<!-- .element: class="fragment" -->
* Capacidad de transporte reducida.<!-- .element: class="fragment" -->
* Difícil realizar un seguimiento.<!-- .element: class="fragment" -->
* Pérdida parcial de mercadería.<!-- .element: class="fragment" -->
* Grandes costos.<!-- .element: class="fragment" -->

</div>
<div class="col fragment">

![before containers](images/contenedores/before-containers.jpg)<!-- .element: class="shadow" -->
</div>
</div>

----
<!-- .slide: data-auto-animate -->

## Analogía con el transporte

Los **contenedores** solucionaron muchos de los problemas:
<div class="container" >

<div class="col" style="min-width: 65%">

* Un vendedor pone todos sus productos en un contenedor y sólo debe
  preocuparse por ese contenedor.<!-- .element: class="fragment" -->
* Los productos nunca se manipulan
  individualmente.<!-- .element: class="fragment" -->
* Tamaños y formas estandarizadas, simplifica toda la cadena de transporte: el
  transporte sólo debe llevar contenedores.<!-- .element: class="fragment" -->

</div>
<div class="col fragment">

![containership](images/contenedores/containership.jpg)<!-- .element: class="shadow" -->
</div>
</div>

----

## ¿Qué son los contenedores de aplicaciones?

* Empaquetan aplicaciones en una unidad estándar de
  intercambio.
* Única pieza de software en un filesystem completo que contiene **todo lo
  necesario** para ejecutar una aplicación: código, librerías, herramientas,
  etc.
* Garantiza que el software **siempre correrá de igual forma** sin importar su
  ambiente.


> Analogía con ROM de juegos
<!-- .element: class="fragment" -->

----

## ¿Por qué usar contenedores?

* Minimiza diferencias entre ambiente de desarrollo y producción.
* Simplifica la instalación de aplicaciones en diferentes plataformas.
* Despliegue de aplicaciones complejas.
* Empaquetado de aplicaciones legadas.

----
<!-- .slide: data-auto-animate -->
## Matriz del infierno

![matrix hell](images/contenedores/the_matrix_of_hell.jpg)<!-- .element: class="shadow" height="550px" -->

----
<!-- .slide: data-auto-animate -->
## Matriz del infierno

![docker matrix hell](images/contenedores/docker-matrix-from-hell.png)<!-- .element: class="shadow" height="550px" -->

----

## Comparación

<div class="container" >
<div class="col">

### Virtualización

![virtualization](images/contenedores/wid-vm.png)<!-- .element: class="shadow" -->

</div>
<div class="col">

### Contenedores

![virtualization](images/contenedores/wid-container.png)<!-- .element: class="shadow" -->

</div>
</div>
----

## Características de los contenedores

<div class="small">

* **Flexibles:** incluso las aplicaciones más complejas pueden conteneirizarse.
* **Livianos**: máximo aprovechamiento del kernel por ser compartido por todos
  los contenedores y el sistema. Esta característica lo vuelve mucho más
  eficiente que cualquier tecnología de virtualización completa.
* **Portables:** construcción local, desplegables en la nube y aptos para correr
  en cualquier plataforma.
* **Débilmente acoplados:** los contenedores son autosuficientes y encapsulados.
  Permiten actualizar un contenedor sin alterar al resto.
* **Escalables**: pueden incrementar o disminuir la cantidad de contenedores
  para un servicio. Además es posible distribuir las réplicas en diferentes
  nodos de un datacenter.
* **Seguros**: aislan las aplicaciones entre sí y de la infraestructura donde corren.

</div>
