SLIDE_FMT: new-topic

# Docker daemon


----
<!-- .slide: data-auto-animate -->
## El servicio docker

El servicio que presta docker en un servidor es conocido como **dockerd**. Una
de las configuraciones por defecto que se establecen, es que el socket donde
recibe comandos utiliza un socker unix en **`/var/run/docker.sock`**.

Puede cambiarse para utilizarse a través de TCP en los puertos **2375**
(inseguro) o **2376** (seguro).

----
<!-- .slide: data-auto-animate -->
## El servicio docker

Las configuraciones del servicio pueden realizarse con opciones al daemon
**dockerd** o mediante un archivo de configuración en formato json que por
defecto se busca en **`/etc/docker/daemon.json`**.

<div class="fragment small">

Puede setear cualquier valor que acepta **dockerd** salvo aquellos que puedan
expresarse como lista de valores en cuyo caso se utiliza el plural del valor que
indica la ayuda. Por ejemplo **label** se expresa como **labels**.
</div>
----
## docker conectando remotamente

* El cliente de docker puede conectar a un servicio remoto:
  * Usando la opción -H tcp://x.y.z.w:2376
  * usando la variable de ambiente **`DOCKER_HOST`**

<div class="fragment">
<div class="small">
A partir de la versión 18.09, el cliente de docker soporta además usar ssh:
</div>

```bash
docker -H ssh://me@example.com ps
```
</div>

----
## docker context

Podemos usar contextos para simplificar y mantener varias conexiones en nuestro
cliente docker:

```
docker context ls
docker context create --help
```

----
<!-- .slide: data-auto-animate -->
## Registries inseguras

Una registry privada puede ser segura o insegura. Que sea segura es porque su
acceso se realiza por medio de TLS y una copia del certificado de la CA es
conocido por el servicio de docker.
  * Las CA conocidas por el servicio docker se almacenan en
    /etc/docker/certs.d/registry.domain:port/ca.crt
  * Una registry es insegura o porque:
    * No usa TLS
    * No conoce la CA de la registry
----
<!-- .slide: data-auto-animate -->
## Registries inseguras

Para poder comunicarse con una registry insegura, el servicio docker
**requiere marcar explícitamente** aquellas registries a las que permtir
conexión en alguna de las siguientes formas:
* `--insecure-registry registry.domain:port`
* `--insecure-registry 10.10.0.0/16`

<div class="small fragment">

Las registries que resuelven a 127.0.0.0/8 son marcadas automáticamente como
inseguras por el servicio de docker.
</div>
----
## Docker detrás de un proxy https

Para que el servicio de docker pueda acceder a docker hub y otras registries a
través de un proxy se utilizan variables de ambiente:

* **`HTTP_PROXY`**
* **`HTTPS_PROXY`**
* **`NO_PROXY`**
----
## Quiénes pueden necesitar proxy

* **Servicio de docker:** como se mencionó antes para acceder a docker hub por
  ejemplo y otras registries.
* **El cliente de docker:** para acceder al daemon docker remoto.
* **El runtime de contenedores:** un contenedor corriendo para acceder a
  servicios HTTP/HTTPS.
