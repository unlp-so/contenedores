SLIDE_FMT: new-topic

# docker cli

----

## docker command

```bash
docker [OPTIONS] COMMAND
```

----
<!-- .slide: data-auto-animate data-transition="fade-out" -->

## run

<div id="animate">

```bash
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

<div class="fragment small">

Corre un contenedor a partir de _IMAGE_
</div>
</div>

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## run

<div id="animate">

```bash
docker run hello-world
```

</div>

----

## Analizamos las imágenes

Las imágenes representan un filesystem para un contenedor. _Es lo que un programa
es a un proceso_.

```bash
docker image ls
```

<div class="fragment small">

**`docker image`** es un subcomando como `docker run`, siendo `ls` una de los
subcomandos de `image`.

Con este comando vemos las imágenes disponibles en
**nuestro** sistema.
</div>


----
<!-- .slide: data-auto-animate data-transition="fade-out" -->

## Lista de contenedores


<div id="animate">

```bash
docker ps
```
</div>

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Lista de contenedores


<div id="animate">

```bash
docker ps --all
```

</div>


----
## Eliminamos los contenedores finalizados

```bash
docker ps -qf status=exited | xargs docker rm
```
<div class="fragment small">

Observamos que **`docker ps`** admite la opción **`-f`** para filtrar contenedores por
[diversos
criterios](https://docs.docker.com/engine/reference/commandline/ps/#filter).
</div>

----
<!-- .slide: data-auto-animate data-transition="fade" -->
## run


```bash
docker run -it ubuntu bash
```

<div class="small">

Seguimos el ejemplo propuesto por **hello-world** usando la imagen oficial de
**ubuntu**.

<div class="fragment">

* La opción **`-i`** mantiene abierto STDIN. Es el modo interactivo.
* La opción **`-t`** creará una pseudo-tty. Combinado con la opción `-i` el uso del
  contenedor es similar al utilizar una consola ssh, xterm o shell.
</div>
</div>

----
<!-- .slide: data-auto-animate data-transition="fade" -->
## run


```bash
docker run -it -e FOO=UNLP -e BAR=Docker \
  --rm ubuntu bash -c 'echo $FOO and $BAR'
```

<div class="small">

* La opción **`-e`** setea una variable de ambiente. _De esta forma se parametrizan configuraciones
inyectándolas en el momento de creación del contenedor._
* La opción **`--rm`** elimina el contenedor cuando termina.
</div>
----
<!-- .slide: data-auto-animate data-transition="fade" -->
## run

Analizamos el código de salida (_exit status_) de un contenedor

```bash
docker run --name test-exit-status -it ubuntu
```
<div class="fragment small">

* La opción **`--name`** nos permite nombrar al contenedor (debe ser único).
* Observamos también que luego de la imagen, en este caso _ubuntu_, no hay
  comando.
* Cuando tengamos el prompt ejecutar `exit 8`.
</div>

----

<!-- .slide: data-auto-animate data-transition="fade" -->
## run

```bash
docker run --name test-exit-status -it ubuntu
```

<div class="fragment small">

Falla porque el nombre **no es único**.
</div>
----

<!-- .slide: data-auto-animate data-transition="fade" -->
## run

```bash
docker run -it ubuntu
```

<div class="fragment small">

Volvemos a salir con otro exit code: `exit 99`.
</div>

----
## Verificamos exit codes con ps

```bash
docker ps -a
```
<div class="fragment small">

Podemos filtrar por exit status **`-f exited=99`**
</div>
----
## Exposición de puertos

Al utilizar un namespace de linux, un contenedor utiliza una red diferente a la
red del host. Por ello no hay forma de acceder al puerto del contenedor desde
una PC externa.

Por ello, **`docker run`** permite exponer puertos:

```bash
docker run --name nginx -d -p 80:80 nginx
curl localhost
```
<div class="small fragment">

Observar **`docker ps`** que muestra el mapeo de puertos
</div>

----
<!-- .slide: data-auto-animate data-transition="fade-out" -->
## logs

```bash
docker logs nginx
docker logs --since 24h nginx
docker logs --tail 10 nginx
docker logs --tail 10 -f nginx
```

<div class="fragment small">

Los logs se muestran en la consola porque es el driver por default (json-file). Sin
embargo es posible configurar otros drivers de logs cuando se corre un
contenedor o para todo el sistema. Algunos driver son none, syslog, gelf, fluentd, etc.

</div>

----
<!-- .slide: data-auto-animate data-transition="fade-out" -->
## logs

Probamos usar syslog

```bash
docker run -it --log-driver syslog \
  --log-opt syslog-address=unixgram:///dev/log \
  alpine echo Hello from docker
```

> Analizar la salida, y qué cambia si usamos `echo -n`
<!-- .element: class="fragment" -->

----
<!-- .slide: data-auto-animate data-transition="fade-out" -->
## Creamos una imagen de forma interactiva

<div id="animate">

* La imagen de ubuntu no provee `curl`
* Lo verificamos primero
* Creamos un contenedor nombrado a partir de la imagen de ubuntu, sin usar
  **`--rm`**, e instalamos curl
* Usaremos **`commit`** para crear a partir de un contenedor (corriendo o
  terminado), una imagen.
* Crearemos otro contenedor a partir de nuestra imagen nueva.

</div>

----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Creamos una imagen de forma interactiva

<div id="animate">

```bash

docker run --rm ubuntu curl -I https://unlp.edu.ar
```

<div class="fragment small">

Falla porque no tiene **`curl`** instalado
</div>
</div>
----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Creamos una imagen de forma interactiva

<div id="animate">

```bash
docker run --name ubuntu-curl ubuntu bash \
  -c 'apt update && apt install -y curl'
```
</div>
----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Creamos una imagen de forma interactiva

<div id="animate">

```bash
docker commit ubuntu-curl unlp-so/ubuntu-curl && \
  docker rm ubuntu-curl
```
<div class="fragment small">

Observar que se crea la imagen **unlp-so/ubuntu-curl**. Podemos verificarlo
con **`docker images ls`**.

Luego de crear la imagen, se elimina el contenedor desde el cuál se creó.

</div>
</div>
----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Creamos una imagen de forma interactiva

<div id="animate">

```bash
docker run --rm unlp-so/ubuntu-curl curl \
  -I https://unlp.edu.ar
```

</div>
----

## Imágenes

* Las imágenes que utilizamos hasta ahora se descargan de un repositorio central
  conocido como [Docker Hub](https://hub.docker.com/).
* Este sitio visibiliza el contenido de la registry oficial de docker:
  **docker.io**.
* Podemos utilizar el subcomando **`search`** buscando imágenes por nombre.
* Podemos descargarlas con **`run`** o **`pull`**.

----

## pull

```bash
docker pull busybox
```
<div class="fragment small">

* Descargará una imagen si no existe en el sistema.
* Verificará si es necesario actualizar la imagen local si ya existe en el
  sistema.
* Si no se especifica **tag** se asume **latest**.
* Si la imagen a descargar es pública no hay inconveniente. Si es privada se
  requiere estar logeuado en la registry usando **`docker login REGISTRY`**.
</div>

----

## push

```bash
docker push unlp-so/ubuntu-curl
```
<div class="fragment small">

* Sube la imagen a la registry.
* La registry se infiere del nombre de la imagen. Las imágenes que son del tipo
  **prefix/name:tag** o **name:tag** asumen como registry **docker.io**.
* Un ejemplo de imagen que utiliza otra registry sería
  **registry.gitlab.com/unlp-so/ubuntu-curl**.
* Para poder realizar esta acción se debe estar logueado en la registry usando
  **`docker login REGISTRY`**
</div>

----

## tag

Podemos renombrar imagenes con **`tag`**

```bash
docker tag unlp-so/ubuntu-curl \
  registry.gitlab.com/unlp-so/ubuntu-curl:1.0.0
```

<div class="fragment small">

El ejemplo muestra además como utilizar un tag diferente a **latest**.
</div>

----
## Operaciones con imágenes

* Las imágenes docker podemos exportarlas con **`save`** e importarlas con
  **`load`**.
* Podemos eliminar una imagen usando **`docker rmi`** ó **`docker image rm`**.
----
<!-- .slide: data-auto-animate -->

## inspect

Este comando lo utilizamos para inspeccionar toda la información acerca de un
contenedor.

```bash
docker run --name dead-curl unlp-so/ubuntu-curl \
  curl -I https://unlp.edu.ar
docker inspect dead-curl
```

----
<!-- .slide: data-auto-animate -->
## inspect

```json
[
    {
        "Id": "be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca",
        "Created": "2020-10-15T22:32:06.119117444Z",
        "Path": "curl",
        "Args": [
            "-I",
            "https://unlp.edu.ar"
        ],
        "State": {
            "Status": "exited",
            "Running": false,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 0,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2020-10-15T22:32:06.503002238Z",
            "FinishedAt": "2020-10-15T22:32:11.690723436Z"
        },
        "Image": "sha256:d77ee86ad1216b2195769682ff819396e59bf263cf499e52f4a9b81275c811b0",
        "ResolvConfPath": "/var/lib/docker/containers/be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca/hostname",
        "HostsPath": "/var/lib/docker/containers/be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca/hosts",
        "LogPath": "/var/lib/docker/containers/be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca/be86896a8bd589d944f737e42831548a451847a76523a336c476d5932b7576ca-json.log",
        "Name": "/dead-curl",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "docker-default",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {},
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Capabilities": null,
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "private",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DeviceRequests": null,
            "KernelMemory": 0,
            "KernelMemoryTCP": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": null,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/ca83f978391c70515a96601f6aa856a6fd2697e3182d83b45bc1bcf4c0d127f0-init/diff:/var/lib/docker/overlay2/85e9d130a2fe9e47c4c24b9f36335d9c3f7637734865419bf5cd0b7786efdd5a/diff:/var/lib/docker/overlay2/3b874fbbbcb5dbf4a57f8f9da0ebfb59647a65da2aacca9099a96b5454611031/diff:/var/lib/docker/overlay2/503f332e2dd98d14039429bafcb77d15bfac075bed1dd54c70d98e3c82de8169/diff:/var/lib/docker/overlay2/c95a82c2bf2b91713f441992fbe3a7986d916c7751b4b0c958f2c25b2ad7c837/diff:/var/lib/docker/overlay2/dcb10492847ca4b57666d8f7e30c09dad76c9fbba839138ee9b35de88c99a40c/diff",
                "MergedDir": "/var/lib/docker/overlay2/ca83f978391c70515a96601f6aa856a6fd2697e3182d83b45bc1bcf4c0d127f0/merged",
                "UpperDir": "/var/lib/docker/overlay2/ca83f978391c70515a96601f6aa856a6fd2697e3182d83b45bc1bcf4c0d127f0/diff",
                "WorkDir": "/var/lib/docker/overlay2/ca83f978391c70515a96601f6aa856a6fd2697e3182d83b45bc1bcf4c0d127f0/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "be86896a8bd5",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": true,
            "AttachStderr": true,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [
                "curl",
                "-I",
                "https://unlp.edu.ar"
            ],
            "Image": "unlp-so/ubuntu-curl",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {}
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "ff1fcd1826c000dc35bab02371f05b1cf9069ed64ed63b9d1e0f1f2dc95b98fb",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {},
            "SandboxKey": "/var/run/docker/netns/ff1fcd1826c0",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "",
            "Gateway": "",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "",
            "IPPrefixLen": 0,
            "IPv6Gateway": "",
            "MacAddress": "",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "b7d47b43dbb03cf3277ecfcea234eded99e4a79945eca36bb92d155d4586124e",
                    "EndpointID": "",
                    "Gateway": "",
                    "IPAddress": "",
                    "IPPrefixLen": 0,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "",
                    "DriverOpts": null
                }
            }
        }
    }
]
```
----
<!-- .slide: data-auto-animate -->

## exec

* Permite acceder a un contenedor corriendo.
* No es una buena práctica abrir un puerto para acceder por ssh.
  * Sólo se corre un proceso por contenedor.
  * Usuarios innecesarios en el contenedor.
* Incluso en contenedores que corren con un usuario determinado, es posible
  cambiar de usuario con `docker exec -u`

----
<!-- .slide: data-auto-animate data-transition="fade-in slide-out" -->

## exec
<div class="small" >

Ya tenemos de un ejemplo anterior, otro contenedor nginx corriendo llamado
**nginx**. Sin embargo instanciamos otro nginx **propio de redhat quay**, debido
que corre como un usuario **no privilegiado**.

</div>

```bash
docker run --name quay-nginx \
  -d quay.io/redhattraining/hello-world-nginx 
docker exec nginx id
docker exec quay-nginx id
docker exec -uroot quay-nginx id
```
<div class="small">

Al igual que con **`docker run`** es posible utilizar los flags **`-i`** y **`-t`**.
</div>
----
<!-- .slide: data-auto-animate -->
## Políticas de reinicio

Permiten especificar cómo un contenedor debería o no ser reiniciado cuando
termina.

Es muy importante considerar el código de retorno del proceso dentro del
contenedor. Por esta razón no se utilizan daemons ni más de un proceso en un
contenedor. Además los procesos deben correr en foreground para que si terminan,
así lo haga el contenedor.

Para definir la política, **`docker run`** admite la opción **`--restart`**.

----
<!-- .slide: data-auto-animate  -->
## Políticas de reinicio

Consideraciones a tener presentes respecto de las políticas:

* Una política tomará efecto luego de iniciar un contenedor de forma exitosa:
  el contenedor debe mantenerse corriendo por al menos 10 segundos y Docker
  pueda monitorizarlo. _Previene un loop infinito_.
* Si un contenedor se para con **`stop`** su política de reinicio quedará sin
  efecto hasta que el **_servicio de docker se reinicie_** o el contenedor se reinicie
  manualmente.
* Las políticas aplican sólo a contenedores. _En servicios swarm se configuran
  de forma diferente_.

----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Políticas de reinicio

* **no**: no iniciar el contenedor cuando termina. *Valor por defecto*.
* **on-failure:[max]**: reiniciar solo si el contenedor termina con exit
  status diferente a **cero**. Limitar opcionalmente los reintentos de reinicio.
* **always**: siempre reiniciar el contenedor. Además el contenedor se iniciará
  cuando inicia el daemon Docker. Incluso aquellos que se hayan parado con
  **`docker stop`**.
* **unless-stopped**: idem anterior, salvo que en un reinicio del servicio
  Docker no iniciará aquellos contenedores que se hayan parado con **`docker stop`**.

----

<!-- .slide: data-auto-animate data-transition="fade" -->
## Políticas de reinicio

Always restart

```bash
docker run --name walking-dead --restart always ubuntu \
  bash -c 'sleep 15 && exit 0'

docker run --name walking-dead-keep-dead --restart unless-stopped ubuntu \
  bash -c 'sleep 15 && exit 0'
```

<div class="small fragment">

La diferencia entre ambas políticas es sutil. **`unless-stopped`** no reinicia
contenedores stopped ante **reinicios del servicio docker**.

Verificar con:

```bash
docker ps -aq | xargs  docker inspect --format '{{ .Name }}: {{ .RestartCount }}'
docker stop walking-dead walking-dead-keep-dead && sudo systemctl restart docker
docker ps -a
```
</div>

----

<!-- .slide: data-auto-animate data-transition="fade" -->
## Políticas de reinicio

On failure

```bash
docker run --name never-restart --restart on-failure ubuntu \
  bash -c 'sleep 15 && exit 0'

docker run --name restart-5 --restart on-failure:5 ubuntu \
  bash -c 'sleep 15 && exit 5'
```

<div class="small fragment">
Verificar con:

```bash
docker ps -n2
docker ps -qn2 | xargs  docker inspect --format '{{ .Name }}: {{ .RestartCount }}'
```
</div>

----

## system

```bash
docker system info # igual a docker info
docker system events
docker system df
docker system prune -a --volumes
```


----
### Un gráfico que resume los comandos

![docker client verbs](images/docker/docker_client_action_verbs.png)<!-- .element: class="shadow" -->

----
## Ciclo de vida de un contenedor

![docker lifecycle](images/docker/docker_lifecycle_container.png)<!-- .element: class="shadow" -->

