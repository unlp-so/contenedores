SLIDE_FMT: new-topic
# Contenedores sin docker

----

## ¿Por qué no usar docker?

Porque a pesar del diseño modular de docker, basado en un cliente y un servidor,
el servidor es monolítico:

* Construye las imágenes
* Push/Pull de imágenes a/de registries
* Corre las imágenes en contenedores

El servicio de docker hace todo.
<!-- .element: class="fragment" -->

> Entonces si falla el servicio, todos sus contenedores serán inalcanzables.
<!-- .element: class="fragment" -->

----
<!-- .slide: data-auto-animate -->

## Alternativas

Si bien docker mantiene esta arquitectura en la que el servidor es monolítico,
en los últimos años ha aportado valiosas contribuciones a la estandarización:

----
<!-- .slide: data-auto-animate -->
## Alternativas

Si bien docker mantiene esta arquitectura en la que el servidor es monolítico,
en los últimos años ha aportado valiosas contribuciones a la estandarización:
  * **[OCI](https://opencontainers.org/):** Open Container Iniciative fue
    iniciada por Docker en 2015. Hoy día es parte de la CNCF.

----
<!-- .slide: data-auto-animate -->
## Alternativas

Si bien docker mantiene esta arquitectura en la que el servidor es monolítico,
en los últimos años ha aportado valiosas contribuciones a la estandarización:
  * **[OCI](https://opencontainers.org/):** Open Container Iniciative fue
    iniciada por Docker en 2015. Hoy día es parte de la CNCF.
  * **[runC](https://github.com/opencontainers/runc):** cli para la gestion de
    contenedores basados en OCI.

----
<!-- .slide: data-auto-animate -->
## Alternativas

Si bien docker mantiene esta arquitectura en la que el servidor es monolítico,
en los últimos años ha aportado valiosas contribuciones a la estandarización:
  * **[OCI](https://opencontainers.org/):** Open Container Iniciative fue
    iniciada por Docker en 2015. Hoy día es parte de la CNCF.
  * **[runC](https://github.com/opencontainers/runc):** cli para la gestion de
    contenedores basados en OCI.
  * **[containerd](https://containerd.io/):** lanzado en 2015 por Docker como un
    daemon para controlar runC.

----
<!-- .slide: data-auto-animate -->
## Alternativas

Y así nacen nuevas alternativas daemonless como:

* [podman](https://podman.io/)
* [buildah](https://buildah.io/)

----
SLIDE_FMT: new-topic

# podman
----
## podman

Su uso es tal cuál docker. Es más, en algunos textos, puede encontrarse la
sugerencia de usar `alias docker=podman`.

```
podman ps
podman run --rm -it docker.io/library/alpine \
  cat /etc/os-release
podman run -d -p 7080:80 --rm -d docker.io/library/nginx:alpine
curl localhost:7080
```


----
## Analizar podman

```
podman info
```

> ¡Corre en modo usuario! A diferencia de docker que se necesitan privilegios de
> root. Incluso cada usuario tendrá su configuración, imágenes y contenedores
> propios.
<!-- .element: class="fragment" -->
----
## Seguro

Al correr como usuario no privilegiado, no es posible abrir un puerto
privilegiado:

```
podman run -d -p 80:80 --rm -d docker.io/library/nginx:alpine
```

> No va a funcionar, pero sí con sudo. Ojo que sudo tendrá otro repositorio de
> contenedores e imagenes propio, uno por cada usuario.
<!-- .element: class="fragment" -->

----
## Comparemos con docker

Podemos comparar, algo que podría suceder si trabajamos con docker en nuestra PC
y con kubernetes usando cri-o. Probar y analizar lo siguiente:

```
podman run -u nobody --rm -it python:alpine python -m http.server 80
docker run -u nobody --rm -it python:alpine python -m http.server 80
```
----
## podman build

Como mencionamos, podemos usar podman como docker. Por ello, también podremos
construir imágenes con `podman build`.

El nombre **Dockerfile** empieza a aparecer como **Containerfile**.

> Es posible incluso ver que la imagen generada con podman podemos cargarla en
> docker: `podman image save IMAGE_NAME | docker load`
<!-- .element: class="fragment" -->

----
SLIDE_FMT: new-topic

# buildah

----

## buildah

* Se especializa en la construcción de imágenes OCI.
* Es una alternativa para **cada uno de los comandos** que se encuentran en
  un `Containerfile`.
* Permite la construcción de imágenes sin privilegios de root.
* Puede usarse desde cualquier lenguaje de scripting para construir imágenes sin
  Containerfile.
* Podman usa librerías de buildah para construir imágenes a partir de
  Containerfile.

----

## buildah con Containerfile

```
buildah build -t buildah-image-from-containerfile .
```

----

## buildah imperativo

```
CID=$(buildah from docker.io/library/alpine:3.17)
buildah config --cmd '["Hello UNLP SO"]' $CID
buildah config --entrypoint '["echo"]' $CID
buildah commit $CID unlp-so-say
```

----

## Otros productos

* **[skopeo](https://github.com/containers/skopeo):** operaciones sobre imagenes y
  repositorios. Funciona con imágenes OCI como Docker V2.
* **[kaniko](https://github.com/GoogleContainerTools/kaniko):** construye
  imagenes de contenedores a partir de un Dockerfile, dentro de contenedores.
  Pensado para usar desde kubernetes.
