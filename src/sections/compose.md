SLIDE_FMT: new-topic

# Compose

----
## ¿Qué es Docker Compose?

* Herramienta / plugin de docker que permite instanciar múltiples contenedores.
* La arquitectura se define y configura en un archivo de texto
  ([YAML](http://yaml.org)).
* Ofrece comandos para:
  * Iniciar, detener y reiniciar servicios.
  * Construir imágenes.
  * Ver el estado de los servicios, los logs, etc.
  * Correr comandos en servicios.

----
## Un `docker-compose.yml`

<div class="small">

```yaml
version: "3.8"
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```

<div class="fragment">

_Puede observarse que se mencionan dos servicios: **web** y **redis**, donde el
primero se construye y el segundo descarga la última imagen de redis_.
</div>
</div>
----
<!-- .slide: data-auto-animate -->
## Características

* **Ambientes aislados en un mismo host:** a través de *proyectos* se aislan los
  ambientes. Si dos proyectos mantienen los mismos nombres de servicios **_evita
  conflictos_**. Incluso correr la misma aplicación en diferentes versiones o
  configuraciones.
* **Preserva volúmenes al crear contenedores:** al instanciar los servicios con
  **`docker compose up`** cualquier volumen previamente creado será reutilizado por
  compose.
----
<!-- .slide: data-auto-animate -->
## Características

* **Recrear contenedores con diferencias:** al reiniciar un contenedor, compose
  mantiene las configuraciones utilizadas anteriormente de forma de reiniciar
  aquellos contenedores que realmente hayan cambiado.
* **Composición y parametrización entre ambientes:** mediante variables de
  ambiente los contenedores pueden ser configurados. Muchas veces las
  configuraciones entre ambientes varían **_sutilmente_**, siendo posible **_combinar
  diferentes compose_** para simplificar esta tarea.
----
## Instalación

Puede instalarse como [plugin de docker o como un
binario](https://docs.docker.com/compose/install/).

----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

* Veremos como crear una aplicación con el framework Flask.
* Crearemos un Dockerfile.
* Probaremos nuestra aplicación con compose.
* Desarrollaremos usando compose.

----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/app.py python 1-100

</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/app.py python 20-50

Define únicamente el **path `/`**
</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/app.py python 9-18

Contabiliza hits
</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/app.py python 7,13

Utiliza redis
</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/app.py python 14-18

_Resiliencia ante la caida o reinicio de redis_
</div>

----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="container">

<div class="col">

CODE: docker-compose/flask-demo/requirements.txt txt
</div>
<div class="col">

Agregamos las dependencias en **`requirements.txt`**
</div>
</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask


CODE: docker-compose/flask-demo/Dockerfile Dockerfile

Creamos el `Dockerfile`

----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

CODE: docker-compose/flask-demo/docker-compose.yml yaml

Por último orquestamos los servicios con compose.

<div class="small fragment">

Instanciamos con **`docker compose up`**
</div>
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

* Lanzar nuestros servicios dispara previamente el build de la imagen.
* Accedemos al servicio http://localhost:5000.
* Analizar la nueva imagen en el sistema con `docker image ls`.
  * La imagen construida lleva el nombre del proyecto como prefijo.
* El ejemplo no es cómodo para desarrollar
  * El código está en la imagen.
----
<!-- .slide: data-auto-animate -->
## Ejemplo con Flask

<div class="small">

CODE: docker-compose/flask-demo/docker-compose.dev.yml yaml


Agregamos otro compose `docker-compose.dev.yml` nuevo que sobrescribe únicamente un volumen de tipo bind
mount para el servicio web. **_El directorio montado es relativo al yml y de
tipo bind mount_.**

</div>

<div class="fragment small">

Lanzamos ahora la aplicación de la siguiente forma:

```
docker compose -f docker-compose.yml \
  -f docker-compose.dev.yml up
```

**Analizar los nombres de los contenedores y redes creados por compose**.
</div>

----
## Wordpress como ejemplo

<div class="small">

CODE: docker-compose/wordpress/docker-compose.yml yaml 1-100
</div>

----
<!-- .slide: data-auto-animate -->
## Wordpress como ejemplo

<div class="small">

CODE: docker-compose/wordpress/docker-compose.yml yaml 4-13


Observamos el servicio **db** corresponde a un MySQL 5.7.
</div>

----
<!-- .slide: data-auto-animate -->
## Wordpress como ejemplo

<div class="small">

CODE: docker-compose/wordpress/docker-compose.yml yaml 15-28


Observamos el servicio **wordpress** y su configuración.
</div>

----
<!-- .slide: data-auto-animate -->
## Wordpress como ejemplo

<div class="small">

CODE: docker-compose/wordpress/docker-compose.yml yaml 6-7,19-20,29-31


Y prestamos especial atención a los volúmenes.
</div>

----
<!-- .slide: data-auto-animate -->
## Wordpress como ejemplo

* Vemos que se utilizan los volúmenes: 
  * **`db_data`**: utilizado para los datos de mysql
  * **`wp_data`**: utilizado por wordpress para los uploads

<div class="container small">

<div class="col fragment">
 
```bash
docker compose up -d
```
Iniciamos los servicios y verificamos la instalación.
</div>
<div class="col fragment">
 
```bash
docker compose down -v
```
Bajamos los servicios y eliminamos los volúmenes. _Probar de reiniciar wordpress
para verificar las diferencias con y sin `-v`_.
</div>

</div>
<div class="fragment small">

**Analizar los nombres de los contenedores, volúmenes y redes creados por compose**.

</div>
----
<!-- .slide: data-auto-animate -->
## Conceptos de compose

* La configuración sirve para definir en forma declarativa la interacción entre contenedores (services).
* La configuración (`docker-compose.yml`) define un proyecto, cuyo nombre es el directorio que contiene dicha configuración.
  * Puede cambiarse con la [variable de ambiente `COMPOSE_PROJECT_NAME`](https://docs.docker.com/compose/reference/envvars/) o la opción -p.
  * El nombre de proyecto se usa como prefijo.
----
<!-- .slide: data-auto-animate -->
## Conceptos de compose

* Hay tres versiones mayores diferentes, la 1, la 2 y la 3.
* Entre la 1 y la 2 no son compatibles entre sí, entre la 2 y la 3 comparten
  estructura, pero se quitan algunas opciones en la 3.
* Se recomienda utilizar la versión 3.
----
<!-- .slide: data-auto-animate -->
## Conceptos de compose

* Compose creará una red propia en docker. Además, podrán asociarse servicios a
  redes definidas en la sección **`networks`**.
* Compose creará volumenes declarados en la sección **`volumes`** de la configuración.
----
<!-- .slide: data-auto-animate -->
## Comandos de compose

<div class="small">

```bash
# Crea redes, volúmenes e inicia los servicios
docker compose up [-d]

# Observar el estado de los servicios
docker compose ps [--services]

# Ver los logs
docker compose logs [-f|--tail=N]

# Para los servicios
docker compose stop
```

</div>
----
<!-- .slide: data-auto-animate -->
## Comandos de compose

<div class="small">

```bash
# Inicia servicios parados. Deben estar creados
docker compose start

# Elimina servicios parados
docker compose rm

# Para los servicio y luego elimina
docker compose down

# Recrea un contenedor
docker compose up --force-recreate [--build]
```

</div>
----
## Construyendo imágenes

* Compose automáticamente construye imágenes cuando se utiliza **`up`** la primera vez
  y no existe una imagen construida.
* **`docker compose build`** permite forzar el build e incluso forzar el pull de la
  imagen origen.
----
## Observaciones

* Es importante usar volúmenes para persistir datos.
* Las redes de cada proyecto son diferentes.
* El uso de **`links`** ya no es necesario.
  * Permitía heredar **env vars** del servicio enlazado así como
    renombrar el servicio. [Más información aquí](https://docs.docker.com/compose/compose-file/#links).
  
* El uso de **`depends_on`** no soluciona la dependencia de servicios.
* Las políticas de reinicio simplifican el problema de qué servicio arranca primero.
