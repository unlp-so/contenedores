SLIDE_FMT: new-topic
# Registry

----

## ¿Qué es la registry?

* Servicio para almacenar y distribuir imágenes de Docker.
* Debe permitir el guardado de imagenes docker referenciadas por un nombre y un
  tag. Por ejemplo `registry.name.domain/image/name` y tags `1.0.0` o `1.1.5`.
* Los usuarios interactúan con la registry mediante pull y push.
* Las descargas y subidas de imágenes pueden resumirse.
* Las registries deberán permitir autorizar el acceso a las imágenes ya sea para
  descargas como subidas.
----

## Resgistries como servicio

* [Docker Hub](https://hub.docker.com/): gratis para imágenes públicas. Limita a
  cantidad de peticiones a la API. Usado en la mayoría de los ejemplos
  anteriores.
* [RedHat Quay](https://quay.io/): sin cargo para imágenes públicas.
* [GitHub Packages](https://github.com/features/packages): sin costo por el
  momento.
* [Gitlab Container
  Registry](https://docs.gitlab.com/ee/user/packages/package_registry/): sin
  costo por el momento, pero con quota de espacio.

----
## Registry privada

Docker pone a disposición una imagen de la registry open source con licencia
Apache. Para utilizarla simplemente:

```bash
docker run -d -p 5000:5000 --rm --name registry registry:2
docker pull ubuntu
docker tag ubuntu localhost:5000/myfirstimage
docker push localhost:5000/myfirstimage
docker pull localhost:5000/myfirstimage
```
