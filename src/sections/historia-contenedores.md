SLIDE_FMT: new-topic

# Precedentes
----
<!-- .slide: data-auto-animate -->

## Línea del tiempo

<div class="mermaid">
  <pre>
    %%{init: { }}}%%
    timeline
          1979: chroot en Unix
          2004: Solaris Containers
          2005: Open VZ
          2006: Process containers (cgroups)
          2008: LXC
          2013: Docker
  </pre>
</div>

----
<!-- .slide: data-auto-animate -->

## Línea del tiempo

<div class="mermaid">
  <pre>
    %%{init: { }}}%%
    timeline
        2014: Kubernetes
        2015: Open Container Initiative (OCI)
        2016: Docker libera containerd: Container Runtime Interface (CRI): Skopeo
        2017: Docker dona containerd a CNCF
        2017: Podman y Buildah
        2019: Containerd y CRI-O comienzan a ser reemplazo de docker shim  
  </pre>
</div>


----
<!-- .slide: data-auto-animate -->

## chroot

Con [chroot](https://man7.org/linux/man-pages/man1/chroot.1.html) se puede
aislar un filesystem:

```
CHROOT=/tmp/chroot-jail
mkdir -p $CHROOT/proc
tar -chf - $(whereis -b bash ls ps tail | cut -d':' -f2)\
  | tar -xvC $CHROOT -f -
tar -chf - $(find $CHROOT -type f -exec ldd {} \;  |grep '/' \
    | sed -r 's@\s+@@;s@.*=> @@;s@ \(.*@@' | sort -u) \
  | tar -xvC $CHROOT -f -
sudo mount -t proc none $CHROOT/proc
sudo chroot $CHROOT /usr/bin/bash
```
----
<!-- .slide: data-auto-animate -->
## chroot

<div class="container small">

<div class="col">

Dentro del jail, podemos ver todos los procesos:

```
ps -ef
```
</div>
<div class="col fragment">

Ahora, si corremos en nuestra PC, no en el ambiente chroot:

```
yes > /dev/null &
```

</div>
</div>
<div class="fragment">

Matamos el proceso de **yes desde chroot**:

```
kill <PID>
```

> ¡Esto no está nada bueno! chroot aisla filesystem, no procesos
<!-- .element: class="fragment" -->

</div>
----
<!-- .slide: data-auto-animate -->
## chroot

Ejemplo para correr alpine desde un minitootfs:

```
CHROOT_ALPINE=/tmp/alpine-3.18
mkdir -p $CHROOT_ALPINE
curl -s https://dl-cdn.alpinelinux.org/alpine/v3.19/releases/x86_64/alpine-minirootfs-3.19.1-x86_64.tar.gz \
  | tar xzf - -C $CHROOT_ALPINE
sudo chroot $CHROOT_ALPINE /bin/ash
```

> Puede verse que tenemos red, no DNS... ¿Por qué?
<!-- .element: class="fragment" -->

----

## Kernel namespaces

Con los [namespaces](https://man7.org/linux/man-pages/man7/namespaces.7.html),
podemos dar las garantías que carece chroot:

* Aislar procesos
* La red
* Puntos de montaje
* Y varios más

La forma de lanzar un proceso asilando namespaces es mediante el comando
**`unshare`**.

----
<!-- .slide: data-auto-animate -->

## unshare

```
sudo unshare -mpfu chroot $CHROOT_ALPINE /bin/ash
```

> Notar que se requiere el uso de sudo por chroot
<!-- .element: class="fragment" -->

----
<!-- .slide: data-auto-animate -->

## unshare

```
unshare -Urmpfu chroot $CHROOT_ALPINE /bin/ash
```

> Estamos creando un rootless container, mapeando el usuario root
<!-- .element: class="fragment" -->
----
<!-- .slide: data-auto-animate -->

## unshare

Podemos montar `/proc`:

```
mount -t proc none /proc
```

Verificar los procesos, la memoria

```
ps -ef && free
```
----
## cgroups

Los cgroups fueron un desarrollo de Google, que originalmente se llamó *process
containers*.

Google recurrió a esta implementación porque su datacenter donde corrían
[Borg](https://static.googleusercontent.com/media/research.google.com/es//pubs/archive/43438.pdf)
carecía de capacidades de virtualización.

Aislar los contenedores con kernel namespaces y chroot, no era suficiente.
Necesitaban además, acotar cuántos recursos consumía cada contenedor.
----

## Versiones de cgroups

* Actualmente hay dos versiones de cgroups: v1 y v2.
* Las nuevas distribuciones Linux proveen cgroups v2.
* Los límites impuestos por cgroups requieren que los procesos hagan caso a los
  límites impuestos, para así no caer en OOMKills por ejemplo.
  * Mencionamos esto por las limitaciones que tuvo java con [cgroups
    v1](https://www.oracle.com/java/technologies/javase/8u191-relnotes.html) y ahora
    con [cgrops v2](https://bugs.openjdk.org/browse/JDK-8230305). Java soporta:
    * cgroups v1 desde java 8u191
    * cgroups v2 desde java 15
    

----
SLIDE_FMT: new-topic-center
## Linux Containers
### LXC
----
<!-- .slide: data-auto-animate -->

## Linux Containers

* Hoy día, LXC es parte de LXD, un daemon para la gestión de ambientes virtuales
  usando virtualización con [qemu](https://www.qemu.org/) y contenedores (lxc).
* El proyecto está respaldado por Canonical (creadores de Ubuntu).
* Basado en el concepto de imagenes remotas que se descargan e instancian en
  alguna de las tecnologías indicadas.

----
<!-- .slide: data-auto-animate -->

## Linux Containers
* Se utilizan *profiles* para configurar ciertas características de las
  instancias, como por ejemplo recursos asignados, dispositivos, red, e incluso
  metadatos de [cloud-init](https://cloudinit.readthedocs.io/).
* Su uso se asocia más a la gestión de máquinas como si fueran virtuales.
  Analogía con AWS EC2, Proxmox vms/containers, VSphere vms.
* La filosofía detrás de lxd es correr ambientes virtuales

----
## Uso de lxc

Asumiendo instalado lxc/lxd, se mostrarán los comandos más comunes para trabajar
con esta herramienta.

> En ubuntu/debian se instala usando `apt install lxc`. La primera vez que se
> ejecute el comando, instalará lxd usando [snapcraft](https://snapcraft.io/).
> La primera vez deberá correrse `lxd init`

----
### LXC remotes

Listando los remotos:

```
lxc remote ls
```

<div class="container small">
<div class="col fragment">

Listando las imágenes de un remoto:

```
lxc image ls ubuntu-minimal:
```

> Si no se especifica el remoto, mostrará las imágenes locales

</div>
<div class="col fragment">

Listando los alias de las imágenes:

```
lxc image alias ls ubuntu-minimal:
```
</div>
</div>
----
### LXC init/launch instance container

Dada una instancia, la creamos con:

```
lxc launch ubuntu-minimal:24.04 ubuntu-24-cdr
```

> Creará un contenedor si se usa `init`, lo creará e iniciará si se usa
> `launch`. El usuario de conexión puede especificarse usando
> [cloud-init](https://cloud-init.io/) desde el perfil.

----
### LXC init/launch instance container

Usando el mismo comando pero con `--vm` en vez de un contenedor tendremos una
vm:

```
lxc launch ubuntu-minimal:24.04 ubuntu-24-vm --vm
```
> En ambos casos podremos ingresar usando `lxc console <INSTANCE> [-t  vga]`.
> O usando `lxc exec <INSTANCE> <CMD>`
----
<!-- .slide: data-auto-animate -->
### LXC profile

* Las máquinas se crearán siguiendo algún perfil.
* Sin el perfil configurado las máquinas deben especificar su configuración o
  toman las del perfil por defecto.

A continuación copiamos el perfil por defecto para crear uno nuevo:

```
lxc profile ls
# Copiamos el perfil
lxc profile copy default default-cloud-config
```


----
<!-- .slide: data-auto-animate -->
### LXC profile

Agregamos la configuración de cloud-init

```
config:
  cloud-init.user-data: |
    #cloud-config
    chpasswd:
      expire: false
      users:
        - name: ubuntu
          password: lxcpass
          type: text
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        shell: /bin/bash
```

> Probar crear una instancia con este perfil usando `-p default-cloud-config`

----
SLIDE_FMT: new-topic-center
## Docker
----
<!-- .slide: data-auto-animate -->

## Docker

* Emerge como proyecto de SL en 2013.
* Se basa en el uso de:
  * **Cgroups** para restringir recursos como cpu, memoria, IO, red, etc.
  * **Kernel namespaces** para aislar información como ya hemos visto. Datos como
    por ejemplo: PID, network, user IDs, unix timesharing system, mounts,
    ipc.
  * [**Filesystem de unión**](https://docs.docker.com/engine/userguide/storagedriver/selectadriver/)
    como es el caso de AUFS, **overlay2**, Btrfs, Device Mapper, ZFS, etc.

> La parte del filesystem le da una vuelta más elegante al chroot
<!-- .element: class="fragment" -->
----
<!-- .slide: data-auto-animate -->

## Docker

* Con las características antes mencionadas se obtienen contenedores
  independientes en una instancia Linux que evita el overhead de manipular VMs.
* Antes de la versión 0.9, Docker usaba **LXC**. A partir de la 0.9
  incorporaron libcontainer, eliminando la dependencia de LXC dado que accede
  directamente al kernel para manipular cgroups, namespaces, apparmor, interfaces
  de red, etc.

----
## Imágenes y contenedores

* Un **contenedor** no es más que **un proceso corriendo** con algunas
  características de encapsulamiento aplicadas para así mantenerlos aislados de
  otros contenedores e incluso del sistema base.
* Una de las características más importantes del aislamiento de contenedores es
  que cada uno mantiene un **_filesystem propio_**. Este filesystem es provisto
  por una **imagen**.

----
<!-- .slide: data-auto-animate -->

## Imágenes y contenedores

* Imagen:
  * Filesystem y parámetros para utilizarla.
  * No cambia nunca y no tiene estados.
* Contenedor:
  * Instancia de una imagen (resultado de ejecutarla).
  * Tiene una capa de RW volátil.

----
<!-- .slide: data-auto-animate data-transition="fade-out" -->
## Imágenes y contenedores

<img id="image" alt="Imagen de Docker" src="images/image-layers.jpg" height="400px" />

----
<!-- .slide: data-auto-animate data-transition="fade" -->
## Imágenes y contenedores

<img id="image" alt="Contenedor de Docker" src="images/container-layers.jpg" height="400px" />

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Imágenes y contenedores

<img id="image" alt="Compartiendo una imagen" src="images/sharing-layers.jpg" height="480px" />

----
<!-- .slide: data-auto-animate data-transition="fade" -->

## Imágenes y contenedores

<img alt="Imagen derivada" src="images/saving-space.png" height="400px" />

----
## Arquitectura de docker

![arquitectura docker](images/docker/architecture.svg)<!-- .element: class="shadow" -->
