SLIDE_FMT: new-topic

# Gestion de recursos

----
## Introducción

* Un contenedor sin límites sobre los recursos podrá utilizar tantos recursos
  como necesite del host donde corra.
* Es posible controlar cuánta memoria y CPU puede consumir un recurso. 
* También pueden limitarse otros recursos como por ejemplo pids.
* Esta funcionalidad se implementa por medio de cgroups.

<div class="small fragment">

**No todos los recursos pueden compartirse. Algunos recursos pueden
sobreutilizarse.**
</div>
----
<!-- .slide: data-auto-animate -->
## Límite de memoria

Para poder probarlo, necesitamos **deshabilitar la swap** debido a que los
contenedores utilizarían la swap cuando no tengan suficiente memoria:

```bash
docker run --rm alpine free  && \
  sudo swapoff -a && \
  docker run --rm alpine free
```

----
<!-- .slide: data-auto-animate -->
## Límite de memoria

Para ejemplificar, probamos con un script PHP que intente abrir un archivo de
10MB

```bash
dd if=/dev/zero count=1 bs=10MB of=/tmp/file-10M
```

Probamos correr un script sin límites:

```
docker run -v /tmp/file-10M:/tmp/file-10M -it \
  php:cli-alpine -r 'file("/tmp/file-10M"); echo "Leido";'
```
----
<!-- .slide: data-auto-animate -->
## Límite de memoria

Para ejemplificar, probamos con un script PHP que intente abrir un archivo de
10MB

```bash
dd if=/dev/zero count=1 bs=10MB of=/tmp/file-10M
```

Probamos correr un script **con límites**:

```
docker run -m 10m -v /tmp/file-10M:/tmp/file-10M -it \
  php:cli-alpine -r 'file("/tmp/file-10M"); echo "Leido";'
```
----
<!-- .slide: data-auto-animate -->
## Límite de memoria

Analizamos el exit status code de los contenedores:

```bash
docker ps -an 2
```
<div class="small fragment">
La salida muestra:

```text
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                       PORTS               NAMES
81ba4dc2a26b        php:cli-alpine      "docker-php-entrypoi…"   6 seconds ago       Exited (137) 5 seconds ago                       adoring_williamson
81699e6b9588        php:cli-alpine      "docker-php-entrypoi…"   14 seconds ago      Exited (0) 13 seconds ago                        charming_snyder
```

**El exit code 137 se recibe cuando un proceso recibe la señal SIGKILL (kill
-l)**.

</div>
----
<!-- .slide: data-auto-animate -->
## Límite CPU

Sin límite

```bash
docker run --rm -it -v /tmp/file-10M:/tmp/file-10M alpine \
  time md5sum /tmp/file-10M
```
----
<!-- .slide: data-auto-animate -->
## Límite CPU

<div class="container small">

<div class="col">

Usando un procesador y medio

```bash
docker run --cpus="1.5" --rm \
  -it -v /tmp/file-10M:/tmp/file-10M \
  alpine time md5sum /tmp/file-10M
```
</div>
<div class="col">

Usando un veinteavo de un procesador

```bash
docker run --cpus="0.05" --rm \
  -it -v /tmp/file-10M:/tmp/file-10M \
  alpine time md5sum /tmp/file-10M
```
</div>
</div>
----
## Límite de PIDs

Podemos limitar cuántos procesos inicia un contenedor:

```bash
docker run --rm --pids-limit 3 -it alpine
```
<div class="fragment">
Y corremos:

```text
echo UNLP | grep U 
echo UNLP | grep U | grep N
```
</div>
