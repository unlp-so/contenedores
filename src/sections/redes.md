SLIDE_FMT: new-topic

# Redes

----
## Introducción

* Los contenedores deben conectarse entre sí.
* Los contenedores deben poder acceder a servicios no conteneirizados.
* La conexión entre contenedores en plataformas Linux o Windows no debe variar.

----
## Drivers de red

* **bridge:** el driver por defecto. Es comúnmente usado en contenedores
  standalone.
* **host:** quita el aislamiento propio de contenedores compartiendo la red con
  el host anfitrión. 
* **overlay:** permiten conectar múltiples docker daemons entre sí y habilitan
  la comunicación entre servicios swarm.
* **macvlan:** permiten asignar una dirección MAC a un contenedor, haciéndola
  ver como un nuevo dispositivo en la red.
* **none:** deshabilita el acceso a la red de un contenedor. Sólo verá la
  interfaz de loopback.
----
## Listamos las redes

```bash
docker network ls
```

<div class="small">

_La red bridge llamada **bridge** es la utilizada por defecto y su uso no es
recomendado para producción. Es preferible utilizar redes bridge definidas por
el usuario._
</div>
----
<!-- .slide: data-auto-animate -->
## Redes bridge

* Crea un bridge por software que permite a los contenedores conectados a ese
  bridge comunicarse y asilarlos de otros que no lo estén.
* Aplican a contenedores que corren en un mismo host. Para comunicar conetedores
  en otros hosts docker se deben usar redes de tipo **overlay** o rutear a nivel
  de SO.
* Las redes de tipo bridge definidas por el usuario proveen resolución de
  nombres automática entre contenedores.
  * En la red por defecto esto **no es así**. En ellas debe  utilizarse **`--link`**
    que no es recomendada.

----
<!-- .slide: data-auto-animate -->
## Redes bridge

* Las redes definidas por el usuario proveen mejor aislamiento porque los
  contenedores se conectan a una red específica ofreciendo mayor control que
  conectando todos los contenedores a una misma red.
* Cada red definida por el usuario instancia u nuevo bridge por software.
* Los contenedores enlazados con `--link` en la red por defecto comparten
  variables de ambiente. 
  * Esto no sucede con las redes definidas por el usuario.
----
<!-- .slide: data-auto-animate -->
## Redes bridge

```bash
docker network create my-network
```
<div class="small">

Observar el nombre de los contenedores crean una entrada en el DNS cuando se
utilizan redes definidas por el usuario:
</div>

<div class="container small">

<div class="col">

```bash
docker run --network my-network \
  --rm -it --name uno alpine
```
<div class="fragment">

```bash
ping dos
```
</div>

</div>

<div class="col">

```bash
docker run --network my-network \
  --rm -it --name dos alpine
```

<div class="fragment">

```bash
ping uno
```
</div>

</div>
</div>
----
## (Des)Conectar un contenedor

Mientras estamos corriendo ping entre los contenedores **uno** y **dos** podemos
verificar esta funcionalidad:

<div class="container">
<div class="col">

```bash
docker network disconnect \
  my-network uno
```
**desconectamos uno**
</div>
<div class="col">

```bash
docker network connect \
  my-network uno
```
**conectamos uno**
</div>
</div>

----

## Publicando puertos

Un contenedor en una red de tipo bridge puede exponer sus puertos:

<div class="container small">
<div class="col">

```bash
docker run --network my-network \
  --rm -d -p 8080:80 nginx
sleep 1 && curl localhost:8080
docker ps -lq | xargs docker rm -f
```

<div class="small">

_Red definida por el usuario_
</div>
</div>
<div class="col">

```bash
docker run --rm -d -p 8080:80 \
  nginx
sleep 1 && curl localhost:8080
docker ps -lq | xargs docker rm -f
```
<div class="small">

_Red por defecto_
</div>

</div>
</div>
----

## Cambiando la red

En las redes definidas por el usuario es posible indicar una subred:

```bash
docker network create my-test-subnet \
  --subnet 192.168.70.0/24 \
```

<div class="small fragment">

Mas información en la documentación de [**`docker network
create`**](https://docs.docker.com/engine/reference/commandline/network_create/)
----

## Red del host

* Un contenedor que use la red del **host** no estará aislado de la red del host
  que comparte la placa física.
* El contenedor no tomará una IP propia, sino que usará la del host.
* Si el contenedor corre un servicio que hace bind al puerto 80, entonces lo
  estará haciendo en el host.
* Sólo funciona en Linux

----

## Ejemplo con red del host

```bash
docker run --network host --rm -d nginx:1-alpine
docker exec -it $(docker ps -lq) ip add ls && \
  curl localhost && \
  docker kill $(docker ps -lq)
```
